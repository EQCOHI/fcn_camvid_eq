import multiprocessing
import os
import sys

from PIL import Image
from joblib import Parallel, delayed


def resize_image(image_path, new_image_path, target_size, do_crop=False):
    try:
        image = Image.open(image_path)

        if do_crop:
            width, height = image.size  # Get dimensions
            min_size = min(width, height)

            left = (width - min_size) // 2
            top = (height - min_size) // 2
            right = (width + min_size) // 2
            bottom = (height + min_size) // 2

            # crop center with square
            image = image.crop((left, top, right, bottom))

        # resize image
        image = image.resize((target_size, target_size))

        # save new image to target directory
        image.save(new_image_path)
    except Exception as e:
        print(e)


def main(argv):
    data_path = argv[0]
    result_path = argv[1]
    target_size = int(argv[2])

    data_path = os.path.expanduser(data_path)
    result_path = os.path.expanduser(result_path)
    os.makedirs(result_path, exist_ok=True)

    image_list = []
    for (path, dir, files) in os.walk(data_path):
        for filename in files:
            ext = os.path.splitext(filename)[-1].lower()
            if ext == '.png' or ext == '.jpg' or ext == '.jpeg':
                image_path = os.path.join(path, filename)
                new_image_path = image_path.replace(data_path, result_path)

                # check new image path
                os.makedirs(os.path.dirname(new_image_path), exist_ok=True)

                image_list.append((image_path, new_image_path))

    num_cores = multiprocessing.cpu_count()
    Parallel(n_jobs=num_cores)(delayed(resize_image)(path, new_path, target_size) for path, new_path in image_list)


if __name__ == '__main__':
    main(sys.argv[1:])
