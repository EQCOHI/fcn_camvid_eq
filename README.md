# Fcn_camvid Yonsei.univ IT comprehensive design

# Train

```
python3 main.py --do_train --train_dir data/train --eval_dir data/val --batch_size 64
```

# Evaluate

```
python3 main.py --do_predict --eval_dir data/val --checkpoint results/pytorch_model_100.bin
```

# Test 
```
python3 main.py --do_test --test_dir data/test --checkpoint results/pytorch_model_100.bin
```

# Download Camvid

```
mkdir data
cd data

wget http://web4.cs.ucl.ac.uk/staff/g.brostow/MotionSegRecData/data/label_colors.txt
wget http://web4.cs.ucl.ac.uk/staff/g.brostow/MotionSegRecData/data/LabeledApproved_full.zip
wget http://web4.cs.ucl.ac.uk/staff/g.brostow/MotionSegRecData/files/701_StillsRaw_full.zip

unzip LabeledApproved_full.zip -d label
unzip 701_StillsRaw_full.zip
cd ..

python3 resize_image.py data/label/ data/label/ 256
python3 resize_image.py data/701_StillsRaw_full/ data/701_StillsRaw_full/ 256

python3 split_train_val.py data/701_StillsRaw_full data/train/raw data/val/raw
python3 split_train_val.py data/label data/train/label data/val/label
```