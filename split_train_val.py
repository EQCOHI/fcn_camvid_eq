import os
import sys
from shutil import copy2

from tqdm import tqdm


def main(argv):
    image_dir = argv[0]
    result_train_dir = argv[1]
    result_val_dir = argv[2]

    # create result directory
    os.makedirs(result_train_dir, exist_ok=True)
    os.makedirs(result_val_dir, exist_ok=True)

    samples = []
    for filename in tqdm(sorted(os.listdir(image_dir)), desc='Reading'):
        ext = os.path.splitext(filename)[-1].lower()
        if ext == '.png' or ext == '.jpg' or ext == '.jpeg':
            image_path = os.path.join(image_dir, filename)
            samples.append((image_path, filename))

    # split dataset
    val_count = len(samples) // 10
    train_samples, val_samples = samples[val_count:], samples[:val_count]

    for image_path, filename in train_samples:
        copy2(image_path, os.path.join(result_train_dir, filename))

    for image_path, filename in val_samples:
        copy2(image_path, os.path.join(result_val_dir, filename))


if __name__ == "__main__":
    main(sys.argv[1:])
