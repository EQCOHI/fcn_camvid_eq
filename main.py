import argparse
import os

import numpy as np
import torch
import torch.nn as nn
from PIL import Image
from torch.utils.data import DataLoader
from torch.utils.data import Dataset
from tqdm import tqdm

from Model2 import Model2


class ImageDataset(Dataset):
    def __init__(self, root, tmp_dir='tmp'):
        os.makedirs(tmp_dir, exist_ok=True)

        self.samples = []

        image_dir = os.path.join(root, 'raw')
        label_dir = os.path.join(root, 'label')
        for filename in tqdm(sorted(os.listdir(image_dir)), desc='Reading'):
            ext = os.path.splitext(filename)[-1].lower()
            if ext == '.png' or ext == '.jpg' or ext == '.jpeg':
                image_path = os.path.join(image_dir, filename)
                label_path = os.path.join(label_dir, filename.split('.')[0] + '_L.' + filename.split('.')[1])
                seg_path = os.path.join(tmp_dir, filename)

                # !!! convert label image(rbg) to segmentation image(black and white)
                label_array = np.array(Image.open(label_path))

                is_road = np.all(label_array == (128, 64, 128), axis=-1)
                label_array[is_road] = 255
                label_array[np.invert(is_road)] = 0

                Image.fromarray(label_array).convert('L').save(seg_path)  # save segmentation

                # append to sample
                self.samples.append((image_path, seg_path))

    def __getitem__(self, index):
        image_filename, seg_filename = self.samples[index]

        # 1) read file
        img = Image.open(image_filename)
        label_img = Image.open(seg_filename)

        # 2) apply transforms
        img = torch.from_numpy(np.array(img).transpose((2, 0, 1))).float().div(127.5).add(-1.0)
        label_img = torch.from_numpy(np.array(label_img)).float().div(127.5).add(-1.0)

        return img, label_img

    def __len__(self):
        return len(self.samples)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--n_epochs", type=int, default=100, help="number of epochs of training")
    parser.add_argument("--batch_size", type=int, default=256, help="size of the batches")
    parser.add_argument("--eval_batch_size", type=int, default=1, help="size of the evaluate batches")
    parser.add_argument("--train_dir", default='', type=str, help="The directory for training.")
    parser.add_argument("--eval_dir", default='', type=str, help="The directory for evaluation.")
    parser.add_argument("--test_dir", default='', type=str, help="The directory for test")
    parser.add_argument("--output_dir", default='results', type=str, help="The output directory.")
    parser.add_argument("--lr", type=float, default=0.0001, help="adam: learning rate")
    parser.add_argument("--n_cpu", type=int, default=4, help="number of cpu threads to use during batch generation")
    parser.add_argument("--img_size", type=int, default=256, help="size of each image dimension")
    parser.add_argument("--do_train", action='store_true', help="Whether to run training.")
    parser.add_argument("--do_predict", action='store_true', help="Whether to run eval on the dev set.")
    parser.add_argument("--do_test", action='store_true', help="only Input.img no label.img")
    parser.add_argument("--checkpoint", default=None, type=str, help="The directory of checkpoint file.")
    parser.add_argument('--gpu0_bsz', type=int, default=-1, help='batch size on gpu 0')
    args = parser.parse_args()

    # expanduser
    args.train_dir = os.path.expanduser(args.train_dir)
    args.eval_dir = os.path.expanduser(args.eval_dir)

    # create output directory
    os.makedirs(args.output_dir, exist_ok=True)

    # get device
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # init model
    in_size, out_size = (3, 1)

    # set dataset and dataloader
    if args.do_train:
        # create model
        model = Model2(in_size=in_size, out_size=out_size, hidden_size=512)

        if args.checkpoint is not None:
            try:
                print("Load checkpoint, Train from {}".format(args.checkpoint))
                model.load_state_dict(torch.load(args.checkpoint), strict=False)  # resume checkpoint
            except Exception as e:
                print(e)

        # Loss function
        criterion = torch.nn.L1Loss()

        # convert to 'cuda' or 'cpu'
        model.to(device)
        criterion.to(device)

        n_gpu = torch.cuda.device_count()
        if n_gpu > 1:
            if args.gpu0_bsz >= 0:
                from data_parallel import BalancedDataParallel

                model = BalancedDataParallel(args.gpu0_bsz, model)
            else:
                model = torch.nn.DataParallel(model)

        train_dataset = ImageDataset(args.train_dir)
        train_dataloader = DataLoader(train_dataset, batch_size=args.batch_size, shuffle=True, num_workers=args.n_cpu)

        # Optimizers
        optimizer = torch.optim.Adam(model.parameters(), lr=args.lr)

        model.train()
        for epoch in range(1, args.n_epochs + 1):
            for step, batch in enumerate(train_dataloader):
                batch = tuple(t.to(device) for t in batch)

                img, label_img = batch[0], batch[1]
                image_output = model(img)

                # calculate loss
                loss = criterion(image_output, label_img)

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

                if step % 100 == 0:
                    print("[Epoch {}/{}] [Batch {}/{}] loss: {}".format(epoch,
                                                                        args.n_epochs,
                                                                        step,
                                                                        len(train_dataloader),
                                                                        loss.item()))

            # save model
            model_to_save = model.module if hasattr(model, 'module') else model
            checkpoint_filename = os.path.join(args.output_dir, "pytorch_model_{}.bin".format(epoch))
            torch.save(model_to_save.state_dict(), checkpoint_filename)

            # set checkpoint
            args.checkpoint = checkpoint_filename

    if args.do_predict:
        # create model
        model = Model2(in_size=in_size, out_size=out_size, hidden_size=512)

        # load checkpoint
        print("Load checkpoint {}".format(args.checkpoint))
        model.load_state_dict(torch.load(args.checkpoint, map_location=device))

        # convert to 'cuda' or 'cpu'
        model.to(device)

        eval_dataset = ImageDataset(args.eval_dir)
        eval_dataloader = DataLoader(eval_dataset, batch_size=args.eval_batch_size, shuffle=False, num_workers=1)

        model.eval()
        for step, batch in enumerate(eval_dataloader):
            batch = tuple(t.to(device) for t in batch)
            img, label_img = batch[0], batch[1]
            image_output = model(img)

            # for image generation
            input_cpu = ((img.data.cpu().numpy() + 1.0) * 127.5).astype(np.uint8)
            label_cpu = ((label_img.data.cpu().numpy() + 1.0) * 127.5).astype(np.uint8)
            output_cpu = ((image_output.data.cpu().numpy() + 1.0) * 127.5).astype(np.uint8)
            for batch_index in range(output_cpu.shape[0]):
                input_image = input_cpu[batch_index][:3, :, :].transpose((1, 2, 0))
                label_image = np.stack((label_cpu[batch_index],) * 3, axis=-1)
                output_image = np.squeeze(np.stack((output_cpu[batch_index],) * 3, axis=-1))

                stack_imgs = np.hstack([input_image, output_image, label_image])
                out_filename = os.path.join(args.output_dir, 'out_{}_{}.png'.format(step, batch_index))
                Image.fromarray(stack_imgs.astype(np.uint8)).save(out_filename)

                print('Save {}'.format(out_filename))

    if args.do_test:
        # create model
        model = Model2(in_size=in_size, out_size=out_size, hidden_size=512)

        # load checkpoint
        print("Load checkpoint {}".format(args.checkpoint))
        model.load_state_dict(torch.load(args.checkpoint, map_location=device))

        # convert to 'cuda' or 'cpu'
        model.to(device)

        test_dataset = ImageDataset(args.test_dir)
        test_dataloader = DataLoader(test_dataset, batch_size=args.eval_batch_size, shuffle=False, num_workers=1)

        model.eval()
        for step, batch in enumerate(test_dataloader):
            batch = tuple(t.to(device) for t in batch)
            img, label_img = batch[0], batch[1]
            image_output = model(img)

            # for image generation
            input_cpu = ((img.data.cpu().numpy() + 1.0) * 127.5).astype(np.uint8)
            # label_cpu = ((label_img.data.cpu().numpy() + 1.0) * 127.5).astype(np.uint8)
            output_cpu = ((image_output.data.cpu().numpy() + 1.0) * 127.5).astype(np.uint8)
            for batch_index in range(output_cpu.shape[0]):
                input_image = input_cpu[batch_index][:3, :, :].transpose((1, 2, 0))
                # label_image = np.stack((label_cpu[batch_index],) * 3, axis=-1)
                output_image = np.squeeze(np.stack((output_cpu[batch_index],) * 3, axis=-1))

                stack_imgs = np.hstack([input_image, output_image])
                out_filename = os.path.join(args.output_dir, 'out_{}_{}.png'.format(step, batch_index))
                Image.fromarray(stack_imgs.astype(np.uint8)).save(out_filename)

                print('Save {}'.format(out_filename))